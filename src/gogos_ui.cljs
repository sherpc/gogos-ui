(ns gogos-ui
  (:require [gogos-ui.widgets.search :as search]
            [gogos-ui.widgets.alert :as alert]
            [gogos-ui.widgets.add-good :as ag]))

(defn log
  [data]
  (.log js/console (str data)))

(let [alert-channels (alert/run-widget "alert")
      ac (:tm alert-channels)]
  (ag/run-widget "add-good" ac)
  (search/run-widget "products-search" ac))

