(ns gogos-ui.widgets.alert
  (:require [gogos-ui.widgets.widget :as w]
            [quiescent :as q :include-macros true]
            [quiescent.dom :as d]
            [cljs.core.async :as a]
            [ajax.core :refer [GET]])
  (:require-macros [cljs.core.async.macros :as am]))

(q/defcomponent Alert
  [data channels]
  (if (:message data)
    (let [{:keys [text type]} (:message data)]
      (d/div {:className (str "alert alert-dismissable alert-" type)}
        (d/button {:type "button" :className "close" :onClick #(am/go (a/>! (:tm channels) :clear))} "×")
        text))
    (d/div {} "")))

(defn set-message
  "Set temp message."
  [state message]
  (if (= :clear message)
    (dissoc state :message)
    (assoc state :message message)))

(defn run-widget
  "Widget entry point"
  [element-id]
  (w/run-widget element-id
                nil
                {:tm set-message}
                nil
                Alert))

