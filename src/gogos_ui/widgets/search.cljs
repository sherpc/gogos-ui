(ns gogos-ui.widgets.search
  (:require [gogos-ui.widgets.widget :as w]
            [quiescent :as q :include-macros true]
            [quiescent.dom :as d]
            [cljs.core.async :as a]
            [ajax.core :refer [GET]])
  (:require-macros [cljs.core.async.macros :as am]))

;; Rendering

(q/defcomponent AjaxLoading
  "Loading ajax gif at center of block."
  []
  (d/div {:className "ajax-loader"} (d/img {:src "resources/images/ajax-loader.gif"})))

(q/defcomponent SearchHeading
  [channel]
  (d/form {}
    (d/input {:type "text"
              :className "form-control"
              :placeholder "Filter..."
              :onChange #(let [v (-> % .-target .-value)] (am/go (a/>! channel v)))})))

(q/defcomponent SearchResult
  [data ac]
  (d/div {}
    (d/span {} "Products")
    (d/hr {})
    (if (nil? data)
      (AjaxLoading)
      (if (empty? data)
        (d/span {} "No results.")
        (apply d/ul {:className "list-group"}
          (map #(d/li {:className "list-group-item" :onClick (fn [e] (let [v (-> e .-target .-innerText)] (am/go (a/>! ac {:text v :type "success"}))))} (:name %)) data))))))

(q/defcomponent SearchPanel
  "Component representing search panel"
  [data channels]
  (d/div {:className "panel panel-default"}
    (d/div {:className "panel-heading"} (SearchHeading (:filter channels)))
    (d/div {:className "panel-body"} (SearchResult (:items data) (:ac data)))))

;; Data flow

(defn api [endpoint] (str "http://localhost:8080/" endpoint "/"))

(defn load-products
  "Load products from API asynchronous and raises callback on success."
  [query success]
  (GET (api "products") {:handler success
                         :params {:query query}
                         :response-format :json
                         :keywords? true}))
(defn set-items
  "Set products."
  [state items]
  (assoc state :items items))

(defn set-products-filter
  "Set products filter."
  [state query channels]
  (load-products query #(am/go (a/>! (:items channels) %)))
  (assoc state :pf query))

;; Binding rendering and data

(defn load-initial-data
  [channels]
  (load-products "" #(am/go (a/>! (:items channels) %))))

(defn run-widget
  "Widget entry point"
  [element-id alert-channel]
  (w/run-widget element-id
                (fn [] {:items nil :filter "" :ac alert-channel})
                {:items set-items
                 :filter set-products-filter}
                load-initial-data
                SearchPanel))
