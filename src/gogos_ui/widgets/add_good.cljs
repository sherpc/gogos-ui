(ns gogos-ui.widgets.add-good
  (:require [gogos-ui.widgets.widget :as w]
            [quiescent :as q :include-macros true]
            [quiescent.dom :as d]
            [cljs.core.async :as a]
            [ajax.core :refer [GET POST]])
  (:require-macros [cljs.core.async.macros :as am]))

;; Rendering

(q/defcomponent AjaxLoading
  "Loading ajax gif at center of block."
  []
  (d/div {:className "ajax-loader"} (d/img {:src "resources/images/ajax-loader.gif"})))

(q/defcomponent AddGoodCategory
  [{:keys [_id name products]} ngc]
  (d/option {:value _id} (str name " (" products ")")))

(q/defcomponent AddGood
  "Component representing add good form"
  [{:keys [categories new-good]} ngc]
  (if (empty? categories)
    (AjaxLoading)
    (d/div {}
      (d/form {:className "form-horizontal"}
        (d/div {:className "form-group"}
          ;;(d/label {:className "col-sm-2 control-label"} (:name new-good))
          (d/div {:className "col-sm-12"}
            (q/on-render (d/input {:className "form-control" :type "text" :onBlur #(let [v (-> % .-target .-value)]
                                                                         (am/go (a/>! ngc {:name v})))})
                         (fn [node] (if-not (:name new-good) (set! (.-value node) ""))))))
          (apply d/select {:className "form-control" :onChange #(let [v (-> % .-target .-value)]
                                                                          (am/go (a/>! ngc {:category v})))}
                 (map #(AddGoodCategory % ngc) categories)))
      (d/button {:className "btn btn-primary" :onClick (fn [_]
                                                         (am/go (a/>! ngc :add))
                                                         )} "Add"))))

(q/defcomponent Panel
  "Component representing panel"
  [data body-component heading-component]
  (d/div {:className "panel panel-default"}
    (d/div {:className "panel-heading"} (heading-component data))
    (d/div {:className "panel-body"} (body-component data))))

(q/defcomponent Root
  [data channels]
  (Panel data #(AddGood % (:ng channels)) (fn [d] (d/div {} (str "Add " (-> d :new-good :name))))))

;; Data flow

(defn api [endpoint] (str "http://localhost:8080/" endpoint "/"))

(defn load-categories
  "Load categories from API asynchronous and raises callback on success."
  [success]
  (GET (api "categories") {:handler success
                           :response-format :json
                           :keywords? true}))
(defn set-categories
  "Set categories."
  [state categories]
  (assoc state :categories categories))

(defn handle-new-good
  [state message channels]
  (if (map? message)
    (assoc state :new-good (merge (:new-good state) message))
    (do
      (POST (api "products") {:handler (fn [_]
                                         (am/go (a/>! (:ac state) {:text "Good added sucessfully." :type "success"}))
                                         (load-categories #(am/go (a/>! (:categories channels) %))))
                              :error-handler (fn [e] (am/go (a/>! (:ac state) {:text (str "Error! " e) :type "danger"})))
                              :format :json
                              :params (:new-good state)
                              :response-format :raw})
      (dissoc state :new-good))))

(defn load-initial-data
  [channels]
  (load-categories #(am/go (a/>! (:categories channels) %))))

(defn run-widget
  "Widget entry point"
  [element-id alert-channel]
  (w/run-widget element-id
                (fn [] {:items nil :filter "" :ac alert-channel})
                {:ng handle-new-good
                 :categories set-categories}
                load-initial-data
                Root))
