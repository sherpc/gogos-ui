(ns gogos-ui.widgets.widget
  (:require [quiescent :as q :include-macros true]
            [cljs.core.async :as a])
  (:require-macros [cljs.core.async.macros :as am]))

(defn load-widget
  "Return a map containing the initial widget"
  [element-id fresh consumers]
  {:element-id element-id
   :state (atom (fresh))
   :channels (into {} (map (fn [k] [k (a/chan)]) (keys consumers)))
   :consumers consumers})

(defn init-updates
  "For every entry in a map of channel identifiers to consumers,
  initiate a channel listener which will update the application state
  using the appropriate function whenever a value is recieved, as
  well as triggering a render."
  [widget renderer]
  (doseq [[ch update-fn] (:consumers widget)]
    (am/go (while true
             (let [val (a/<! (get (:channels widget) ch))
                   _ (.log js/console (str "on widget " (:element-id widget) ", on channel [" ch "], recieved value [" val "]"))
                   new-state (swap! (:state widget) update-fn val (:channels widget))]
               (renderer widget))))))


(defn run-widget
  "Widget entry point. Needs:
   -- element-id: DOM element-id to render
   -- fresh: function, what will create initial widget data
   -- consumers: map of channels keys and it's consumers. Channels will be created automatically.
   -- load-initial-data: function, what loads intial data. Will be invoked with channels of widgets.
   -- component: quiescent root component to render widget."
  [element-id fresh consumers load-initial-data component]
  (let [render-pending? (atom false)
        renderer (fn [widget]
                  (when (compare-and-set! render-pending? false true)
                  (.requestAnimationFrame js/window
                                          (fn []
                                            (q/render (component @(:state widget) (:channels widget))
                                                      (.getElementById js/document (:element-id widget)))
                                            (reset! render-pending? false)))))
        widget (load-widget element-id (if fresh fresh (fn [] {})) consumers)]
      (init-updates widget renderer)
      (when load-initial-data (load-initial-data (:channels widget)))
      (renderer widget)
      (:channels widget)))
