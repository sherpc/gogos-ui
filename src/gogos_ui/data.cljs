(ns gogos-ui.data
  (:require [cljs.core.async :as a]
            [ajax.core :refer [GET POST]])
  (:require-macros [cljs.core.async.macros :as am]))

(defn api [endpoint] (str "http://localhost:8080/" endpoint "/"))

(defn load-categories
  "Load categories from API asynchronous and raises callback on success."
  [success]
  (GET (api "categories") {:handler success
                           :response-format :json
                           :keywords? true}))

(defn set-categories
  "Set categories."
  [state categories]
  (assoc state :categories categories))

(defn set-message
  "Set temp message."
  [state message]
  (if (= :clear message)
    (dissoc state :message)
    (assoc state :message message)))

(defn log
  [data]
  (.log js/console (str data)))

(defn handle-new-good
  [state message channels]
  (if (map? message)
    (assoc state :new-good (merge (:new-good state) message))
    (do
      (POST (api "products") {:handler (fn [_]
                                         (am/go (a/>! (:message channels) {:text "Good added sucessfully." :type "success"}))
                                         (load-categories #(am/go (a/>! (:cat channels) %))))
                              :error-handler (fn [e] (am/go (a/>! (:message channels) {:text (str "Error! " e) :type "danger"})))
                              :format :json
                              :params (:new-good state)
                              :response-format :raw})
      (dissoc state :new-good))))



(def static-categories ["Whiskey" "Milk" "Fruits"])

(def search-panel {:body {:search-type "Categories" :results static-categories}})
(def selected-good {:heading "Selected good! Testing CD" :body "Component should be here"})

(defn fresh
  "Returns a new, empty application state."
  []
  {:search-panel search-panel
   :right-panel selected-good
   :categories []})

