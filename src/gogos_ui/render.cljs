(ns gogos-ui.render
  (:require [quiescent :as q :include-macros true]
            [cljs.core.async :as a]
            [quiescent.dom :as d])
  (:require-macros [cljs.core.async.macros :as am]))

(q/defcomponent AjaxLoading
  "Loading ajax gif at center of block."
  []
  (d/div {:className "ajax-loader"} (d/img {:src "resources/images/ajax-loader.gif"})))

(q/defcomponent AddGoodCategory
  [{:keys [_id name products]} ngc]
  (d/option {:value _id} (str name " (" products ")")))

(q/defcomponent AddGood
  "Component representing add good form"
  [{:keys [categories new-good]} ngc]
  (if (empty? categories)
    (AjaxLoading)
    (d/div {}
      (d/form {:className "form-horizontal"}
        (d/div {:className "form-group"}
          ;;(d/label {:className "col-sm-2 control-label"} (:name new-good))
          (d/div {:className "col-sm-12"}
            (q/on-render (d/input {:className "form-control" :type "text" :onBlur #(let [v (-> % .-target .-value)]
                                                                         (am/go (a/>! ngc {:name v})))})
                         (fn [node] (if-not (:name new-good) (set! (.-value node) ""))))))
          (apply d/select {:className "form-control" :onChange #(let [v (-> % .-target .-value)]
                                                                          (am/go (a/>! ngc {:category v})))}
                 (map #(AddGoodCategory % ngc) categories)))
      (d/button {:className "btn btn-primary" :onClick (fn [_]
                                                         (am/go (a/>! ngc :add))
                                                         )} "Add"))))


(q/defcomponent Label
  [data]
  (d/span {:className "label label-primary"} data))

(q/defcomponent Panel
  "Component representing panel"
  [data body-component heading-component]
  (d/div {:className "panel panel-default"}
    (d/div {:className "panel-heading"} (heading-component data))
    (d/div {:className "panel-body"} (body-component data))))

(q/defcomponent Alert
  [{:keys [text type]} channel]
  (d/div {:className (str "alert alert-dismissable alert-" type)}
    (d/button {:type "button" :className "close" :onClick #(am/go (a/>! channel :clear))} "×")
    text))

(q/defcomponent Grid
  "Component representing grid"
  [data channels]
  (d/div {}
    (Panel data #(AddGood % (:ng channels)) (fn [d] (d/div {} (str "Add " (-> d :new-good :name)))))))


;; Here we use an atom to tell us if we already have a render queued
;; up; if so, requesting another render is a no-op
(let [render-pending? (atom false)]
  (defn request-render
    "Render the given application state tree."
    [app]
    (when (compare-and-set! render-pending? false true)
      (.requestAnimationFrame js/window
                              (fn []
                                (q/render (Grid @(:state app) (:channels app))
                                          (.getElementById js/document "add-good-d"))
                                (reset! render-pending? false))))))
